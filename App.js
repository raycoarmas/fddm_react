import React from 'react';
import { Text, View, Image, Button, TextInput, Alert} from 'react-native';
import { Constants } from 'expo';

//https://upload.wikimedia.org/wikipedia/commons/d/de/Bananavarieties.jpg
export default class LoadImage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
      uri: 'a'
   }
  }
  render() {
    return (
      <View style={{padding: 10}}>
        <TextInput
          style={{height: 40}}
          placeholder="Write!"
          onChangeText={(text) => this.setState({text})}
        />
        <Button
          onPress={() => {
            this.setState({ uri: this.state.text })
          }}
          title="Search"
        />
        <Image source={{uri: this.state.uri}} style={{width: 193, height: 110}}/>
        
      </View>
    );
  }

}